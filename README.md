# BinaryRotator

What do you get if combine _John the Ripper_, _GNU Grep_ and _Julius Caesar_?  
Simple: **BinaryRotator**

```
Usage: BinaryRotator.jar [-cfhkloprVx] [-d=<amount>] [-t=<threads>] [-s=<strings>]... [FILES...]
Grep-like GNU/Linux utility for performing a brute force caesar cipher on files.
      [FILES...]            files to search through.
  -c, --case                enables case sensitive search.
  -d, --dump=<amount>       how many rows of bytes to dump.
  -f, --suppress            suppresses all not found messages.
  -h, --help                Show this help message and exit.
  -k, --overview            prints an overview at the end.
  -l, --no-links            disable following of links when recursing.
  -o, --only-shifted        only scan for shifted matches.
  -p, --plain               disables all text color/decoration.
  -r, --recursive           recursivly scans directories.
  -s, --string=<strings>    string to search for.
  -t, --threads=<threads>   how many threads to analyze with.
  -V, --version             Print version information and exit.
  -x, --hexadecimal         display dump in hexadecimal.
```
