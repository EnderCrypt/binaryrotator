/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.source.sources.failed;


import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.FileSystemException;
import java.nio.file.NoSuchFileException;
import java.util.Objects;

import endercrypt.binaryrotator.bytes.ByteSequence;
import endercrypt.binaryrotator.source.AbstractDataSource;
import endercrypt.binaryrotator.source.DataSource;
import endercrypt.binaryrotator.source.DataSourceException;


public class FailedDataSource extends AbstractDataSource
{
	public static FailedDataSource resolve(DataSource dataSource, IOException exception)
	{
		return resolve(dataSource.getName(), exception);
	}
	
	public static FailedDataSource resolve(String name, IOException exception)
	{
		Objects.requireNonNull(name, "name");
		Objects.requireNonNull(exception, "exception");
		if (exception instanceof NoSuchFileException)
		{
			return new FailedDataSource.NoSuchFile(name, exception);
		}
		if (exception instanceof AccessDeniedException)
		{
			return new FailedDataSource.InsufficientReadPermissions(name, exception);
		}
		if (exception instanceof IOException && Objects.equals(exception.getMessage(), "Is a directory"))
		{
			return new FailedDataSource.IsDirectory(name, exception);
		}
		if (exception instanceof FileSystemException)
		{
			return new FailedDataSource.UnexpectedFilesystemIOException(name, exception);
		}
		return new FailedDataSource.UnexpectedIOException(name, exception);
	}
	
	public static class NoSuchFile extends FailedDataSource
	{
		public NoSuchFile(String name, Throwable exception)
		{
			super(name, "No such file", exception);
		}
	}
	
	public static class InsufficientReadPermissions extends FailedDataSource
	{
		public InsufficientReadPermissions(String name, Throwable exception)
		{
			super(name, "insufficient read permissions", exception);
		}
	}
	
	public static class IsDirectory extends FailedDataSource
	{
		public IsDirectory(String name, Throwable exception)
		{
			super(name, "Is a directory", exception);
		}
	}
	
	public static class UnexpectedFilesystemIOException extends FailedDataSource
	{
		public UnexpectedFilesystemIOException(String name, Throwable exception)
		{
			super(name, "unexpected filesystem I/O exception", exception);
		}
	}
	
	public static class UnexpectedIOException extends FailedDataSource
	{
		public UnexpectedIOException(String name, Throwable exception)
		{
			super(name, "unexpected I/O exception", exception);
		}
	}
	
	private final String message;
	private final Throwable exception;
	
	public FailedDataSource(String name, String message, Throwable exception)
	{
		super(name);
		this.message = Objects.requireNonNull(message, "message");
		this.exception = exception;
	}
	
	public DataSourceException generateException()
	{
		return new DataSourceException(this, message, exception);
	}
	
	@Override
	public ByteSequence collectBytes() throws DataSourceException
	{
		throw generateException();
	}
}
