/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.source.sources.file;


import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.binaryrotator.application.Application;
import endercrypt.binaryrotator.application.Arguments;
import endercrypt.binaryrotator.application.ExitCode;
import endercrypt.binaryrotator.bytes.ByteSequence;
import endercrypt.binaryrotator.source.AbstractDataSource;
import endercrypt.binaryrotator.source.DataSource;
import endercrypt.binaryrotator.source.DataSourceException;
import endercrypt.binaryrotator.source.sources.failed.FailedDataSource;


public class FileDataSource extends AbstractDataSource
{
	private static final Logger logger = LogManager.getLogger(FileDataSource.class);
	
	public static List<DataSource> collect(Arguments arguments)
	{
		List<DataSource> sources = new ArrayList<>();
		for (Path path : arguments.getFiles())
		{
			sources.addAll(collect(arguments, path));
		}
		return sources;
	}
	
	private static List<DataSource> collect(Arguments arguments, Path file)
	{
		if (arguments.isRecursive())
		{
			// file visit options
			EnumSet<FileVisitOption> fileVisitOptions = EnumSet.noneOf(FileVisitOption.class);
			if (arguments.isNoLinks() == false)
			{
				fileVisitOptions.add(FileVisitOption.FOLLOW_LINKS);
			}
			
			try
			{
				// visit
				FileDataSourceCollector fileCollector = new FileDataSourceCollector();
				Files.walkFileTree(file, fileVisitOptions, Integer.MAX_VALUE, fileCollector);
				return fileCollector.getDataSources();
			}
			catch (IOException e)
			{
				logger.error("Failed to recurse through " + file, e);
				ExitCode.IO_EXCEPTION.endApplication();
				return null;
			}
		}
		else
		{
			return List.of(new FileDataSource(file));
		}
	}
	
	private final Path file;
	
	public FileDataSource(Path file)
	{
		super(file.toString());
		this.file = Application.workDir.resolve(file).normalize();
	}
	
	public Path getFile()
	{
		return file;
	}
	
	@Override
	public ByteSequence collectBytes() throws DataSourceException
	{
		try
		{
			logger.debug("Attempting to read " + getFile() + " into a " + DataSource.class.getSimpleName());
			return ByteSequence.fromArray(Files.readAllBytes(getFile()));
		}
		catch (IOException e)
		{
			throw FailedDataSource.resolve(this, e).generateException();
		}
	}
}
