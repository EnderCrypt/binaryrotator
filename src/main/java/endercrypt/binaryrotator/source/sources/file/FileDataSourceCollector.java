/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.source.sources.file;


import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.binaryrotator.source.DataSource;
import endercrypt.binaryrotator.source.sources.failed.FailedDataSource;


public class FileDataSourceCollector implements FileVisitor<Path>
{
	private static final Logger logger = LogManager.getLogger(FileDataSourceCollector.class);
	
	private List<DataSource> dataSources = new ArrayList<>();
	
	public List<DataSource> getDataSources()
	{
		return Collections.unmodifiableList(dataSources);
	}
	
	@Override
	public FileVisitResult preVisitDirectory(Path directory, BasicFileAttributes attributes) throws IOException
	{
		return FileVisitResult.CONTINUE;
	}
	
	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) throws IOException
	{
		dataSources.add(new FileDataSource(file));
		return FileVisitResult.CONTINUE;
	}
	
	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exception) throws IOException
	{
		logger.trace("Visiting file failed", exception);
		dataSources.add(FailedDataSource.resolve(new FileDataSource(file), exception));
		return FileVisitResult.CONTINUE;
	}
	
	@Override
	public FileVisitResult postVisitDirectory(Path directory, IOException exception) throws IOException
	{
		logger.trace("Visiting directory failed", exception);
		return FileVisitResult.CONTINUE;
	}
}
