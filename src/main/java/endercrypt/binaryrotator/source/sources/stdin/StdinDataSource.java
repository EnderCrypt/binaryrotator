/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.source.sources.stdin;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.binaryrotator.bytes.ByteSequence;
import endercrypt.binaryrotator.source.AbstractDataSource;
import endercrypt.binaryrotator.source.DataSource;
import endercrypt.binaryrotator.source.DataSourceException;
import endercrypt.binaryrotator.source.sources.failed.FailedDataSource;


public class StdinDataSource extends AbstractDataSource
{
	private static final Logger logger = LogManager.getLogger(StdinDataSource.class);
	
	public static final String STDIN_NAME = "[STDIN]";
	
	public static Optional<DataSource> request()
	{
		InputStream source = System.in;
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		try
		{
			int c = -1;
			while ((source.available() > 0) && (c = source.read()) != -1)
			{
				buffer.write(c);
			}
		}
		catch (IOException e)
		{
			return Optional.of(new FailedDataSource(STDIN_NAME, "Unexpected I/O exception", e));
		}
		if (buffer.size() == 0)
		{
			return Optional.empty();
		}
		else
		{
			logger.debug("Read " + buffer.size() + " bytes from STDIN");
			return Optional.of(new StdinDataSource(buffer.toByteArray()));
		}
	}
	
	private byte[] bytes;
	
	protected StdinDataSource(byte[] bytes)
	{
		super(STDIN_NAME);
		this.bytes = bytes;
	}
	
	@Override
	public ByteSequence collectBytes() throws DataSourceException
	{
		return ByteSequence.fromArray(bytes);
	}
}
