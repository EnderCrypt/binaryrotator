/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.source;


import com.github.tomaslanger.chalk.Chalk;


public class DataSourceException extends Exception
{
	private static final long serialVersionUID = 6594996732138375992L;
	
	/**
	 * 
	 */
	
	private final DataSource dataSource;
	
	public DataSourceException(DataSource dataSource, String message)
	{
		super(message);
		this.dataSource = dataSource;
	}
	
	public DataSourceException(DataSource dataSource, String message, Throwable cause)
	{
		super(message, cause);
		this.dataSource = dataSource;
	}
	
	public DataSource getDataSource()
	{
		return dataSource;
	}
	
	public String getTerminalErrorMessage()
	{
		return Chalk.on("Failed to analyze file").bgRed().white() + ": " + dataSource.getName() + " (" + getMessage() + ")";
	}
}
