/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.bytes;

public class SubByteSequence extends ByteSequence
{
	private final ByteSequence bytes;
	private final int start;
	private final int end;
	
	protected SubByteSequence(ByteSequence bytes, int start, int end)
	{
		this.bytes = bytes;
		this.start = start;
		this.end = end;
		if (start < 0 || start >= bytes.getLength())
		{
			throw new IllegalArgumentException("index start out of range: " + start);
		}
		if (end < 0 || end >= bytes.getLength())
		{
			throw new IllegalArgumentException("index end out of range: " + start);
		}
		if (end <= start)
		{
			throw new IllegalArgumentException("end needs to be a higher number than start");
		}
	}
	
	@Override
	public byte getAsByte(int index)
	{
		if (index < 0 || index >= getLength())
		{
			throw new ArrayIndexOutOfBoundsException(index);
		}
		return bytes.getAsByte(start + index);
	}
	
	@Override
	public int getLength()
	{
		return end - start;
	}
}
