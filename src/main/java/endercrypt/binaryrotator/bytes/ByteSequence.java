/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.bytes;


import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public abstract class ByteSequence
{
	private static final Logger logger = LogManager.getLogger(ByteSequence.class);
	
	public static ByteSequence fromArray(byte[] bytes)
	{
		logger.debug("Read " + bytes.length + " bytes into a " + ByteSequence.class.getSimpleName());
		
		return new ByteSequence()
		{
			@Override
			public byte getAsByte(int index)
			{
				return bytes[index];
			}
			
			@Override
			public int getLength()
			{
				return bytes.length;
			}
		};
	}
	
	public abstract byte getAsByte(int index);
	
	public int getAsInt(int index)
	{
		return Byte.toUnsignedInt(getAsByte(index));
	}
	
	public abstract int getLength();
	
	public IntStream asIntStream()
	{
		return IntStream
			.range(0, getLength())
			.map(this::getAsByte);
	}
	
	public byte[] asRawBytes()
	{
		byte[] result = new byte[getLength()];
		for (int i = 0; i < getLength(); i++)
		{
			result[i] = getAsByte(i);
		}
		return result;
	}
	
	public RotatedByteSequence asRotatedSequence(int rotation)
	{
		return new RotatedByteSequence(this, rotation);
	}
	
	public SubByteSequence asSubSequence(int start, int end)
	{
		return new SubByteSequence(this, start, end);
	}
	
	public String asAnalyticsString()
	{
		return asTranslatedString(new IntFunction<String>()
		{
			@Override
			public String apply(int value)
			{
				if (value < 32 || value > 126)
				{
					return ".";
				}
				return String.valueOf((char) value);
			}
		});
	}
	
	public String asTranslatedString(IntFunction<String> translator)
	{
		return asIntStream()
			.mapToObj(translator::apply)
			.collect(Collectors.joining(""));
	}
	
	public String asString()
	{
		return asTranslatedString((value) -> String.valueOf((char) value));
	}
}
