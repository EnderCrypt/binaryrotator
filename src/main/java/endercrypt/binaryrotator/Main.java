/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.tomaslanger.chalk.Chalk;

import endercrypt.binaryrotator.analyzer.AnalyzerResult;
import endercrypt.binaryrotator.analyzer.DataAnalyzer;
import endercrypt.binaryrotator.application.Arguments;
import endercrypt.binaryrotator.application.ExitCode;
import endercrypt.binaryrotator.source.DataSource;
import endercrypt.binaryrotator.source.DataSourceException;
import endercrypt.binaryrotator.source.sources.file.FileDataSource;
import endercrypt.binaryrotator.source.sources.stdin.StdinDataSource;
import endercrypt.binaryrotator.utility.LogOrganizer;


public class Main
{
	private static final Logger logger = LogManager.getLogger(Main.class);
	
	public static void main(Arguments arguments)
	{
		// logger
		LogOrganizer.configure(arguments);
		
		// terminal color
		if (arguments.isPlain())
		{
			Chalk.setColorEnabled(false);
		}
		
		// sources
		List<DataSource> sources = new ArrayList<>();
		
		// stdin
		StdinDataSource.request().ifPresent(sources::add);
		
		// files
		sources.addAll(FileDataSource.collect(arguments));
		
		// sources check
		logger.info("Detected " + sources.size() + " data sources");
		if (sources.size() == 0)
		{
			System.err.println(Chalk.on("No sources or files specified").bgRed().white());
			ExitCode.NO_SOURCES.endApplication();
			return;
		}
		
		// analyze
		Map<DataSource, AnalyzerResult> results = new HashMap<>();
		Map<DataSource, String> failedSources = new HashMap<>();
		for (DataSource source : sources)
		{
			logger.debug("Scanning " + source.getName() + " ...");
			try
			{
				AnalyzerResult result = DataAnalyzer.analyze(arguments, source);
				result.printResult();
				results.put(source, result);
			}
			catch (DataSourceException e)
			{
				failedSources.put(source, e.getMessage());
				System.err.println(e.getTerminalErrorMessage());
				logger.debug("Failed to analyze file: " + e.getDataSource().getName(), e);
			}
			catch (Exception e)
			{
				logger.error("Failed to analyze due to unexpected exception: " + source.getName(), e);
				ExitCode.UNCAUGHT_EXCEPTION.endApplication();
			}
		}
		logger.info("Analyzed " + sources.size() + " files");
		
		// overview
		if (arguments.isOverview())
		{
			System.out.println("Scanned " + results.size() + " file" + (results.size() == 1 ? "" : "s"));
			System.out.println("Useless files: " + results.values().stream().filter(ar -> ar.getMatchCount() == 0).count());
			System.out.println("Matched Files: " + results.values().stream().filter(ar -> ar.getMatchCount() > 0).count());
			if (failedSources.size() > 0)
			{
				System.out.println("Broken files: ");
				failedSources.entrySet().stream()
					.map(e -> " - " + e.getKey().getName() + " (" + e.getValue() + ")")
					.forEach(System.out::println);
			}
		}
	}
}
