/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.utility;

public class ByteCharacterComparer
{
	public static boolean compare(byte b1, byte b2)
	{
		return b1 == b2;
	}
	
	public static boolean compare(boolean caseSensitive, byte b1, byte b2)
	{
		if (caseSensitive == false)
		{
			b1 = toUpperCase(b1);
			b2 = toUpperCase(b2);
		}
		return compare(b1, b2);
	}
	
	private static byte toUpperCase(byte b)
	{
		return (byte) (b >= 97 && b <= 122 ? (b - 32) : b);
	}
}
