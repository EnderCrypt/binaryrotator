/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.utility.resources;


import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;


public class Resources
{
	public interface InputStreamConverter<T>
	{
		public T convert(InputStream stream) throws IOException;
	}
	
	public static <T> T readAs(String path, InputStreamConverter<T> converter)
	{
		Objects.requireNonNull(converter);
		
		Path filePath = Paths.get("/").resolve(path).normalize();
		try (InputStream input = Resources.class.getResourceAsStream(filePath.toString()))
		{
			if (input == null)
			{
				throw new ResourceException(filePath);
			}
			return converter.convert(input);
		}
		catch (IOException e)
		{
			throw new ResourceException(filePath, e);
		}
	}
	
	public static byte[] readAsBytes(String path)
	{
		return readAs(path, InputStream::readAllBytes);
	}
	
	public static String readAsString(String path)
	{
		return new String(readAsBytes(path));
	}
	
	public static List<String> readAsLines(String path)
	{
		return Arrays.asList(readAsString(path).split("\n"));
	}
}
