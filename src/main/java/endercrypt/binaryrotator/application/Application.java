/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.application;


import java.io.PrintWriter;
import java.lang.Thread.UncaughtExceptionHandler;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import endercrypt.binaryrotator.Main;
import endercrypt.binaryrotator.utility.YamlMaster;
import endercrypt.binaryrotator.utility.resources.Resources;
import picocli.CommandLine;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.ParameterException;
import picocli.CommandLine.ParseResult;


public class Application
{
	private static Logger logger = LoggerFactory.getLogger(Application.class);
	
	public static final Path jarFilePath = Paths.get(Application.class.getProtectionDomain().getCodeSource().getLocation().getPath());
	public static final String jarFilename = Optional.ofNullable(jarFilePath.getFileName()).map(Path::toString).orElse(null);
	
	public static final Path workDir = Paths.get(System.getProperty("user.dir"));
	
	public static final Information information = YamlMaster.parse(Resources.readAsString("./information.yaml"), Information.class);
	
	private static final boolean printVersionOnStartup = false;
	
	public static void main(String[] args) throws Exception
	{
		// jar file
		if (jarFilename == null)
		{
			logger.error("Failed to pinpoint source jar file");
			ExitCode.INITIALIZATION_PROBLEM.endApplication();
			return;
		}
		
		// launch arguments
		Arguments arguments = new Arguments();
		CommandLine commandLine = new CommandLine(arguments);
		commandLine.setCommandName(information.getName());
		commandLine.getCommandSpec().usageMessage().description(information.getDescription());
		commandLine.setUsageHelpAutoWidth(true);
		CommandSpec commandSpec = commandLine.getCommandSpec();
		commandSpec.name(jarFilename);
		commandSpec.versionProvider(information.getVersion().getPicoliVersionProvider());
		
		ParseResult result = parsePicocli(commandLine, arguments, args);
		
		// help
		if (result.isUsageHelpRequested())
		{
			commandLine.usage(commandLine.getOut());
			return;
		}
		if (result.isVersionHelpRequested())
		{
			commandLine.printVersionHelp(commandLine.getOut());
			return;
		}
		
		// info
		if (printVersionOnStartup)
		{
			logger.info(Application.information.getName() + " " + Application.information.getVersion());
		}
		
		// uncaught exception
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler()
		{
			@Override
			public void uncaughtException(Thread thread, Throwable exception)
			{
				String threadName = thread.getName();
				
				logger.error("Thread " + threadName + " crashed with uncaught exception", exception);
				ExitCode.UNCAUGHT_EXCEPTION.endApplication();
				return;
			}
		});
		
		// Main
		Main.main(arguments);
	}
	
	private static ParseResult parsePicocli(CommandLine commandLine, Arguments arguments, String[] args)
	{
		try
		{
			ParseResult parseResult = commandLine.parseArgs(args);
			arguments.verify();
			return parseResult;
		}
		catch (ParameterException e)
		{
			// writer
			PrintWriter writer = commandLine.getErr();
			
			// error
			writer.write(commandLine.getColorScheme().errorText(e.getMessage()).toString() + "\n");
			writer.flush();
			
			// help
			commandLine.usage(writer);
			writer.flush();
			
			// exit
			ExitCode.UNCAUGHT_EXCEPTION.endApplication(false);
			return null;
		}
	}
}
