/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.application;


import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Level;

import picocli.CommandLine.Command;
import picocli.CommandLine.ITypeConverter;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;


@Command(mixinStandardHelpOptions = true, helpCommand = true)
public class Arguments
{
	protected Arguments()
	{
		// protected
	}
	
	@Option(names = { "-s", "--string" }, arity = "1", description = "string to search for.")
	private List<String> strings;
	
	public List<String> getStrings()
	{
		return Collections.unmodifiableList(strings);
	}
	
	@Option(names = { "-c", "--case" }, description = "enables case sensitive search.")
	private boolean caseSensitive = false;
	
	public boolean isCaseSensitive()
	{
		return caseSensitive;
	}
	
	@Option(names = { "-o", "--only-shifted" }, required = false, description = "only scan for shifted matches.")
	private boolean onlyShifted = false;
	
	public boolean isOnlyShifted()
	{
		return onlyShifted;
	}
	
	@Option(names = { "-d", "--dump" }, paramLabel = "<amount>", required = false, description = "how many rows of bytes to dump.")
	private int dumpRows = 0;
	
	public int getDumpRows()
	{
		return dumpRows;
	}
	
	@Option(names = { "-x", "--hexadecimal" }, required = false, description = "display dump in hexadecimal.")
	private boolean hexadecimal = false;
	
	public boolean isHexadecimal()
	{
		return hexadecimal;
	}
	
	@Option(names = { "-f", "--suppress" }, required = false, description = "suppresses all not found messages.")
	private boolean suppress = false;
	
	public boolean isSuppress()
	{
		return suppress;
	}
	
	@Option(names = { "-r", "--recursive" }, required = false, description = "recursivly scans directories.")
	private boolean recursive = false;
	
	public boolean isRecursive()
	{
		return recursive;
	}
	
	@Option(names = { "-p", "--plain" }, required = false, description = "disables all text color/decoration.")
	private boolean plain = false;
	
	public boolean isPlain()
	{
		return plain;
	}
	
	@Option(names = { "-k", "--overview" }, required = false, description = "prints an overview at the end.")
	private boolean overview = false;
	
	public boolean isOverview()
	{
		return overview;
	}
	
	@Option(names = { "-t", "--threads" }, required = false, description = "how many threads to analyze with.")
	private int threads = 1;
	
	public int getThreads()
	{
		return threads;
	}
	
	@Option(names = { "-l", "--no-links" }, required = false, description = "disable following of links when recursing.")
	private boolean noLinks;
	
	public boolean isNoLinks()
	{
		return noLinks;
	}
	
	@Option(names = { "--log-level" }, required = false, hidden = true, converter = LogLevelConverter.class, description = "change the log level.")
	private Optional<Level> logLevel = null;
	
	public Optional<Level> getLogLevel()
	{
		return logLevel;
	}
	
	private static class LogLevelConverter implements ITypeConverter<Level>
	{
		@Override
		public Level convert(String value) throws Exception
		{
			Level level = Level.getLevel(value.toUpperCase());
			if (level == null)
			{
				throw new picocli.CommandLine.TypeConversionException("Unknown LogLevel");
			}
			return level;
		}
	}
	
	@Parameters(paramLabel = "FILES", arity = "0..*", description = "files to search through.")
	private List<Path> files = List.of();
	
	public List<Path> getFiles()
	{
		Function<Path, Path> converter = new Function<Path, Path>()
		{
			@Override
			public Path apply(Path path)
			{
				if (path.getRoot() == null)
				{
					return Paths.get("./").resolve(Application.workDir.relativize(Application.workDir.resolve(path)));
				}
				else
				{
					return path.normalize();
				}
			}
		};
		
		// normalize
		return files.stream()
			.map(converter::apply)
			.collect(Collectors.toList());
	}
	
	public void verify()
	{
		// log level
		getLogLevel();
		
		// threads
		if (getThreads() < 1)
		{
			throw new IllegalArgumentException("cannot use " + getThreads() + " threads");
		}
	}
}
