/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.application;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.text.WordUtils;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExitCode implements Comparable<ExitCode>
{
	private static Logger logger = LoggerFactory.getLogger(ExitCode.class);
	
	private static List<ExitCode> exitCodes = new ArrayList<>();
	
	// success
	public static final ExitCode SUCCESS = new ExitCode("The command executed successfully");
	
	// unix 
	public static final ExitCode UNIX_GENERAL_ERROR = new ExitCode("(UNIX) Catchall for general errors");
	public static final ExitCode UNIX_MISUSE_SHELL = new ExitCode("(UNIX) Misuse of shell builtins");
	
	// general
	public static final ExitCode BAD_CMD_ARGS = new ExitCode("Bad command-line arguments");
	public static final ExitCode UNCAUGHT_EXCEPTION = new ExitCode("Uncaught exception");
	public static final ExitCode INTERRUPTED = new ExitCode("Process thread unexpectedly interrupted");
	public static final ExitCode INITIALIZATION_PROBLEM = new ExitCode("Initialization of program failed");
	public static final ExitCode IO_EXCEPTION = new ExitCode("I/O failure");
	public static final ExitCode NO_SOURCES = new ExitCode("no data sources specified");
	
	static
	{
		Collections.sort(exitCodes);
	}
	
	private static int failure_code_counter = 0;
	
	public static List<ExitCode> getExitCodes()
	{
		return Collections.unmodifiableList(exitCodes);
	}
	
	private int code;
	private String description;
	
	private ExitCode(String description)
	{
		this(failure_code_counter++, description);
	}
	
	private ExitCode(int code, String description)
	{
		this.code = code;
		this.description = WordUtils.capitalize(description);
		
		exitCodes.add(this);
	}
	
	public int getCode()
	{
		return code;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	@Override
	public int compareTo(ExitCode other)
	{
		return Integer.compare(other.getCode(), this.getCode());
	}
	
	public void endApplication()
	{
		endApplication(true);
	}
	
	public void endApplication(boolean verbose)
	{
		if (verbose)
		{
			logger.info("Exit: ( " + getCode() + " ) " + getDescription());
		}
		LogManager.shutdown();
		System.exit(getCode());
	}
}
