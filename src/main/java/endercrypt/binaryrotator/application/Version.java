/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.application;


import java.io.Serializable;

import org.apache.commons.text.WordUtils;

import com.github.tomaslanger.chalk.Chalk;

import picocli.CommandLine.IVersionProvider;


public class Version implements Serializable
{
	private static final long serialVersionUID = 7570800193611660738L;
	
	/**
	 * 
	 */
	
	public static Version fromArray(int[] version)
	{
		int i = 0;
		return new Version(version[i++], version[i++], version[i++]);
	}
	
	private static final String SEPARATOR = ".";
	
	private static final String[] MAJOR_NAMES = {
		"Alpha", // 0
		"Caesar of Unix" // 1
	};
	
	private final int major;
	private final int minor;
	private final int patch;
	
	public Version(int major, int minor, int patch)
	{
		this.major = major;
		this.minor = minor;
		this.patch = patch;
		
		if (getMajor() > MAJOR_NAMES.length - 1)
		{
			throw new IllegalArgumentException("Major version " + getMajor() + " does not have a name");
		}
	}
	
	public int getMajor()
	{
		return major;
	}
	
	public int getMinor()
	{
		return minor;
	}
	
	public int getPatch()
	{
		return patch;
	}
	
	public String getName()
	{
		return WordUtils.capitalize(MAJOR_NAMES[getMajor()]).trim();
	}
	
	public String toVersionNumbers()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getMajor()).append(SEPARATOR);
		sb.append(getMinor()).append(SEPARATOR);
		sb.append(getPatch());
		return sb.toString();
	}
	
	public IVersionProvider getPicoliVersionProvider()
	{
		return new IVersionProvider()
		{
			@Override
			public String[] getVersion() throws Exception
			{
				return new String[] {
					Chalk.on(Application.information.getName() + " " + Version.this.toString()).bgWhite().black().toString(),
					"Created by " + Application.information.getAuthor(),
					"Repository: " + Chalk.on(Application.information.getRepository()).underline() };
			}
		};
	}
	
	@Override
	public String toString()
	{
		return toVersionNumbers() + " \"" + getName() + "\"";
	}
}
