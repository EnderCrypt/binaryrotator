/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.analyzer;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.binaryrotator.analyzer.thread.AnalyzerThread;
import endercrypt.binaryrotator.analyzer.thread.AnalyzerThreadResult;
import endercrypt.binaryrotator.application.Arguments;
import endercrypt.binaryrotator.application.ExitCode;
import endercrypt.binaryrotator.bytes.ByteSequence;
import endercrypt.binaryrotator.bytes.RotatedByteSequence;
import endercrypt.binaryrotator.source.DataSource;
import endercrypt.binaryrotator.source.DataSourceException;


public class DataAnalyzer
{
	private static final Logger logger = LogManager.getLogger(DataAnalyzer.class);
	
	public static AnalyzerResult analyze(Arguments arguments, DataSource source) throws ExecutionException, DataSourceException
	{
		DataAnalyzer dataAnalyzer = new DataAnalyzer(arguments, source);
		ExecutorService executor = Executors.newFixedThreadPool(arguments.getThreads());
		try
		{
			return dataAnalyzer.analyze(executor);
		}
		catch (InterruptedException e)
		{
			logger.error("Analyzer thread was unexpectedly interrupted", e);
			ExitCode.INTERRUPTED.endApplication();
			return null;
		}
		finally
		{
			executor.shutdown();
		}
	}
	
	private final Arguments arguments;
	private final DataSource dataSource;
	private final ByteSequence bytes;
	
	public DataAnalyzer(Arguments arguments, DataSource dataSource) throws DataSourceException
	{
		this.arguments = arguments;
		this.dataSource = dataSource;
		this.bytes = dataSource.collectBytes();
	}
	
	public Arguments getArguments()
	{
		return arguments;
	}
	
	public DataSource getDataSource()
	{
		return dataSource;
	}
	
	public ByteSequence getBytes()
	{
		return bytes;
	}
	
	private AnalyzerResult analyze(ExecutorService executor) throws InterruptedException, ExecutionException
	{
		// setup threads
		List<AnalyzerThread> tasks = new ArrayList<>();
		for (String string : arguments.getStrings())
		{
			logger.debug("Searching for \"" + string + "\"");
			
			int start = arguments.isOnlyShifted() ? 1 : 0;
			for (int rotation = start; rotation < 256; rotation++)
			{
				if (arguments.isCaseSensitive() == false && (rotation == 256 - 32 || rotation == 32))
				{
					continue; // skip 224 and 32 as this is where capitalization matches up
				}
				
				RotatedByteSequence rotatedBytes = bytes.asRotatedSequence(rotation);
				tasks.add(new AnalyzerThread(arguments, rotation, string, rotatedBytes));
			}
		}
		
		// execute
		List<AnalyzerThreadResult> results = new ArrayList<>();
		for (Future<AnalyzerThreadResult> future : executor.invokeAll(tasks))
		{
			results.add(future.get());
		}
		
		// result
		return new AnalyzerResult(this, results);
	}
}
