/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.analyzer;


import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.tomaslanger.chalk.Chalk;

import endercrypt.binaryrotator.analyzer.thread.AnalyzerThreadResult;


public class AnalyzerResult
{
	private static final Logger logger = LogManager.getLogger(AnalyzerResult.class);
	
	private final DataAnalyzer dataAnalyzer;
	private final List<AnalyzerThreadResult> threadResults;
	private final int matchCount;
	
	public AnalyzerResult(DataAnalyzer dataAnalyzer, List<AnalyzerThreadResult> threadResults)
	{
		this.dataAnalyzer = dataAnalyzer;
		
		this.threadResults = new ArrayList<>(threadResults);
		
		this.matchCount = threadResults.stream()
			.mapToInt(tr -> tr.getMatches().size())
			.sum();
	}
	
	public DataAnalyzer getFileAnalyzer()
	{
		return dataAnalyzer;
	}
	
	public List<AnalyzerThreadResult> getThreadResults()
	{
		return threadResults;
	}
	
	public int getMatchCount()
	{
		return matchCount;
	}
	
	public void printResult()
	{
		String dataSourceName = getFileAnalyzer().getDataSource().getName();
		if (getMatchCount() > 0)
		{
			for (AnalyzerThreadResult analyzerThreadResult : getThreadResults())
			{
				analyzerThreadResult.printFound(dataSourceName);
			}
		}
		else
		{
			logger.debug("no matches found for " + getFileAnalyzer().getDataSource().getName());
			if (getFileAnalyzer().getArguments().isSuppress() == false)
			{
				System.err.println(Chalk.on(dataSourceName).bgWhite().black() + ": " + Chalk.on("NOT FOUND").red());
			}
		}
	}
}
