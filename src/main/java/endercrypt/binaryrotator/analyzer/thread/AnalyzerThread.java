/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.analyzer.thread;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.binaryrotator.application.Arguments;
import endercrypt.binaryrotator.bytes.ByteSequence;
import endercrypt.binaryrotator.bytes.RotatedByteSequence;
import endercrypt.binaryrotator.utility.ByteCharacterComparer;


public class AnalyzerThread implements Callable<AnalyzerThreadResult>
{
	private static final Logger logger = LogManager.getLogger(AnalyzerThread.class);
	
	private final Arguments arguments;
	private final int rotation;
	private final String string;
	private final RotatedByteSequence bytes;
	
	public AnalyzerThread(Arguments arguments, int rotation, String string, RotatedByteSequence bytes)
	{
		this.arguments = arguments;
		this.rotation = rotation;
		this.string = string;
		this.bytes = bytes;
	}
	
	public Arguments getArguments()
	{
		return arguments;
	}
	
	public int getRotation()
	{
		return rotation;
	}
	
	public String getString()
	{
		return string;
	}
	
	public ByteSequence getBytes()
	{
		return bytes;
	}
	
	@Override
	public AnalyzerThreadResult call() throws Exception
	{
		logger.trace("Scanning through byte rotation: " + rotation);
		
		List<Integer> matches = new ArrayList<>();
		int start = 0;
		int index = -1;
		while (start < bytes.getLength() && (index = findSequence(bytes, start, string)) != -1)
		{
			matches.add(index);
			start = index + 1;
		}
		
		return new AnalyzerThreadResult(this, matches);
	}
	
	private int findSequence(RotatedByteSequence rotatedBytes, int start, String text)
	{
		byte[] chars = text.getBytes();
		for (int bi = start; bi < rotatedBytes.getLength() - text.length(); bi++)
		{
			boolean matches = true;
			for (int ti = 0; ti < chars.length; ti++)
			{
				byte b = rotatedBytes.getAsByte(bi + ti);
				byte c = chars[ti];
				boolean match = ByteCharacterComparer.compare(getArguments().isCaseSensitive(), b, c);
				if (match == false)
				{
					matches = false;
					break;
				}
			}
			if (matches)
			{
				return bi;
			}
		}
		return -1;
	}
}
