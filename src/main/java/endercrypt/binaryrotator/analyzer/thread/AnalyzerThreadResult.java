/************************************************************************
 * BinaryRotator by EnderCrypt (Magnus Gunnarsson)                      *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.analyzer.thread;


import java.util.Collections;
import java.util.List;

import com.github.tomaslanger.chalk.Chalk;

import endercrypt.binaryrotator.application.Arguments;
import endercrypt.binaryrotator.bytes.ByteSequence;
import endercrypt.binaryrotator.dump.ByteDump;
import endercrypt.binaryrotator.dump.builder.ByteDumperBuilder;
import endercrypt.binaryrotator.dump.utility.ByteDumpFormat;
import endercrypt.binaryrotator.dump.utility.CharacterRange;


public class AnalyzerThreadResult
{
	private final AnalyzerThread analyzerThread;
	private final List<Integer> matches;
	
	public AnalyzerThreadResult(AnalyzerThread analyzerThread, List<Integer> matches)
	{
		this.analyzerThread = analyzerThread;
		this.matches = matches;
	}
	
	public AnalyzerThread getAnalyzerThread()
	{
		return analyzerThread;
	}
	
	public List<Integer> getMatches()
	{
		return Collections.unmodifiableList(matches);
	}
	
	public void printFound(String name)
	{
		for (int index : getMatches())
		{
			printFound(name, index);
		}
	}
	
	private void printFound(String name, int index)
	{
		Arguments arguments = getAnalyzerThread().getArguments();
		String string = getAnalyzerThread().getString();
		ByteSequence bytes = getAnalyzerThread().getBytes();
		int rotation = getAnalyzerThread().getRotation();
		
		String found = bytes.asSubSequence(index, index + string.length()).asString();
		String rotationString = rotation == 0 ? "no rotation" : "rotation " + rotation;
		System.out.println(Chalk.on(name).bgWhite().black() + ": " + Chalk.on("found string \"" + found + "\" with " + rotationString + " on index " + index).green());
		int dumpRows = arguments.getDumpRows();
		if (dumpRows > 0)
		{
			ByteDump byteDump = new ByteDumperBuilder()
				.setBytes(bytes)
				.setRowAmount(dumpRows)
				.setFormat(arguments.isHexadecimal() ? ByteDumpFormat.HEXADECIMAL : ByteDumpFormat.DECIMAL)
				.setCharacterRange(CharacterRange.string(index, string))
				.build();
			
			byteDump.print();
		}
		System.out.println();
	}
}
