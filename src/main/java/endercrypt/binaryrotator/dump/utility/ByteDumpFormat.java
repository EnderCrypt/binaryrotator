/************************************************************************
 * BinaryRotator by EnderCrypt                                          *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.dump.utility;


import org.apache.commons.lang3.StringUtils;


public enum ByteDumpFormat
{
	DECIMAL(10, String::valueOf),
	HEXADECIMAL(16, (value) -> {
		String hexString = Integer.toHexString(value).toUpperCase();
		String paddedHexString = StringUtils.leftPad(hexString, 2, " ");
		return paddedHexString;
	});
	
	private final int radix;
	private final ByteDumpPrinter printer;
	private final int maxLength;
	
	private ByteDumpFormat(int radix, ByteDumpPrinter printer)
	{
		this.radix = radix;
		this.printer = printer;
		this.maxLength = printer.print(255).length();
	}
	
	public int getRadix()
	{
		return radix;
	}
	
	public ByteDumpPrinter getPrinter()
	{
		return printer;
	}
	
	public int getMaxLength()
	{
		return maxLength;
	}
}
