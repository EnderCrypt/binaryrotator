/************************************************************************
 * BinaryRotator by EnderCrypt                                          *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.dump.utility;

public class CharacterRange
{
	public static CharacterRange string(int index, String string)
	{
		return range(index, index + string.length());
	}
	
	public static CharacterRange range(int start, int end)
	{
		return new CharacterRange(start, end);
	}
	
	private final int start;
	private final int end;
	
	private CharacterRange(int start, int end)
	{
		this.start = start;
		this.end = end;
		if (start < 0)
		{
			throw new IllegalArgumentException("start is less than 0");
		}
		if (start > end)
		{
			throw new IllegalArgumentException("start is more than end");
		}
	}
	
	public int getStart()
	{
		return start;
	}
	
	public int getEnd()
	{
		return end;
	}
	
	public boolean isInside(int index)
	{
		return (index > start && index < end - 1);
	}
	
	public boolean isTouching(int index)
	{
		return (index >= start && index <= end - 1);
	}
}
