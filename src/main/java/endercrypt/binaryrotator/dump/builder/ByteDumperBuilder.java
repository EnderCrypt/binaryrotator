/************************************************************************
 * BinaryRotator by EnderCrypt                                          *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.dump.builder;


import endercrypt.binaryrotator.bytes.ByteSequence;
import endercrypt.binaryrotator.dump.ByteDump;
import endercrypt.binaryrotator.dump.utility.ByteDumpFormat;
import endercrypt.binaryrotator.dump.utility.CharacterRange;


public class ByteDumperBuilder
{
	// bytes
	private ByteSequence bytes;
	
	public ByteDumperBuilder setBytes(ByteSequence bytes)
	{
		this.bytes = bytes;
		return this;
	}
	
	// rows
	private Integer rowAmount;
	
	public ByteDumperBuilder setRowAmount(int rowAmount)
	{
		this.rowAmount = rowAmount;
		return this;
	}
	
	// format
	private ByteDumpFormat format;
	
	public ByteDumperBuilder setFormat(ByteDumpFormat format)
	{
		this.format = format;
		return this;
	}
	
	// characters
	private CharacterRange characterRange;
	
	public ByteDumperBuilder setCharacterRange(CharacterRange characterRange)
	{
		this.characterRange = characterRange;
		return this;
	}
	
	// build
	public ByteDump build()
	{
		ByteDumperSettings settings = new ByteDumperSettings(bytes, rowAmount, format, characterRange);
		return new ByteDump(settings);
	}
}
