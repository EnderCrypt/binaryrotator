/************************************************************************
 * BinaryRotator by EnderCrypt                                          *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.dump.builder;


import java.util.Objects;

import endercrypt.binaryrotator.bytes.ByteSequence;
import endercrypt.binaryrotator.dump.utility.ByteDumpFormat;
import endercrypt.binaryrotator.dump.utility.CharacterRange;


public class ByteDumperSettings
{
	private final ByteSequence bytes;
	private final int rowAmount;
	private final ByteDumpFormat format;
	private final CharacterRange characterRange;
	
	public ByteDumperSettings(ByteSequence bytes, Integer rowAmount, ByteDumpFormat format, CharacterRange characterRange)
	{
		this.bytes = Objects.requireNonNull(bytes, "bytes");
		
		this.rowAmount = Objects.requireNonNull(rowAmount, "rowAmount");
		if (this.rowAmount <= 0)
		{
			throw new IllegalArgumentException("rowAmount cant be zero or less");
		}
		
		this.format = Objects.requireNonNull(format, "format");
		
		this.characterRange = Objects.requireNonNull(characterRange, "characterRange");
	}
	
	public ByteSequence getBytes()
	{
		return bytes;
	}
	
	public int getRowAmount()
	{
		return rowAmount;
	}
	
	public ByteDumpFormat getFormat()
	{
		return format;
	}
	
	public CharacterRange getCharacterRange()
	{
		return characterRange;
	}
}
