/************************************************************************
 * BinaryRotator by EnderCrypt                                          *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.dump.character;


import endercrypt.binaryrotator.dump.builder.ByteDumperSettings;


public class StandardByteDumpCharacter extends ByteDumpCharacter
{
	protected StandardByteDumpCharacter(ByteDumperSettings settings, int index)
	{
		super(settings, index);
		if (OutOfBoundsByteDumpCharacter.isOutOfBounds(settings, index))
		{
			throw new IllegalArgumentException("index out off bounds");
		}
	}
	
	public int getValue()
	{
		return getSettings().getBytes().getAsInt(getIndex());
	}
	
	@Override
	public String getByteString()
	{
		return getSettings().getFormat().getPrinter().print(getValue());
	}
	
	@Override
	public String getReadableCharacter()
	{
		char c = (char) getValue();
		if (c >= ' ' && c <= 126)
		{
			return String.valueOf(c);
		}
		return ByteDumpCharacter.FALLBACK_CHARACTER;
	}
}
