/************************************************************************
 * BinaryRotator by EnderCrypt                                          *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.dump.character;


import com.github.tomaslanger.chalk.Chalk;

import endercrypt.binaryrotator.dump.builder.ByteDumperSettings;


public class MatchedStandardByteDumpCharacter extends StandardByteDumpCharacter
{
	protected MatchedStandardByteDumpCharacter(ByteDumperSettings settings, int index)
	{
		super(settings, index);
	}
	
	@Override
	protected Chalk modify(String text)
	{
		Chalk chalk = super.modify(text);
		chalk.underline();
		return chalk;
	}
}
