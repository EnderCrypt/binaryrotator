/************************************************************************
 * BinaryRotator by EnderCrypt                                          *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.dump.character;


import com.github.tomaslanger.chalk.Chalk;

import endercrypt.binaryrotator.dump.builder.ByteDumperSettings;


public abstract class ByteDumpCharacter
{
	public static ByteDumpCharacter create(ByteDumperSettings settings, int index)
	{
		// out of bounds
		if (OutOfBoundsByteDumpCharacter.isOutOfBounds(settings, index))
		{
			return new OutOfBoundsByteDumpCharacter(settings, index);
		}
		
		// matched string
		if (settings.getCharacterRange().isTouching(index))
		{
			return new MatchedStandardByteDumpCharacter(settings, index);
		}
		
		// standard characters
		return new StandardByteDumpCharacter(settings, index);
	}
	
	public static final String FALLBACK_CHARACTER = ".";
	
	private final ByteDumperSettings settings;
	private final int index;
	
	protected ByteDumpCharacter(ByteDumperSettings settings, int index)
	{
		this.settings = settings;
		this.index = index;
	}
	
	public ByteDumperSettings getSettings()
	{
		return settings;
	}
	
	public int getIndex()
	{
		return index;
	}
	
	public boolean isMatchCharacter()
	{
		return settings.getCharacterRange().isTouching(getIndex());
	}
	
	protected Chalk modify(String text)
	{
		return Chalk.on(text);
	}
	
	private String adjustedString(String text, int size)
	{
		int paddingRequired = size - text.length();
		if (paddingRequired < 0)
		{
			throw new IllegalArgumentException("\"" + text + "\" overflows expected string length of <" + size);
		}
		return modify(text).toString() + (" ".repeat(paddingRequired));
	}
	
	protected abstract String getByteString();
	
	public String asByteString(int size)
	{
		return adjustedString(getByteString(), size);
	}
	
	protected abstract String getReadableCharacter();
	
	public String asReadableCharacter()
	{
		return adjustedString(getReadableCharacter(), 1);
	}
}
