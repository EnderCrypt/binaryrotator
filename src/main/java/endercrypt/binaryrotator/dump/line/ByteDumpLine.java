/************************************************************************
 * BinaryRotator by EnderCrypt                                          *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.dump.line;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import endercrypt.binaryrotator.dump.builder.ByteDumperSettings;
import endercrypt.binaryrotator.dump.character.ByteDumpCharacter;


public class ByteDumpLine
{
	private final ByteDumperSettings settings;
	private final int row;
	private final List<ByteDumpCharacter> characters;
	
	public ByteDumpLine(ByteDumperSettings settings, int row)
	{
		this.settings = settings;
		this.row = row;
		this.characters = new ArrayList<>();
		
		int start = (getRow() * getSettings().getFormat().getRadix());
		int end = start + getSettings().getFormat().getRadix();
		
		for (int index = start; index < end; index++)
		{
			characters.add(ByteDumpCharacter.create(settings, index));
		}
	}
	
	public ByteDumperSettings getSettings()
	{
		return settings;
	}
	
	public int getRow()
	{
		return row;
	}
	
	public List<ByteDumpCharacter> getCharacters()
	{
		return Collections.unmodifiableList(characters);
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		
		// line number
		int index = (row * getSettings().getFormat().getRadix());
		String indexString = getSettings().getFormat().getPrinter().print(index);
		int lastRow = (getSettings().getCharacterRange().getEnd() / getSettings().getFormat().getRadix()) + getSettings().getRowAmount();
		int lastIndexLength = String.valueOf(lastRow * getSettings().getFormat().getRadix()).length();
		sb.append(StringUtils.rightPad(indexString, lastIndexLength, " "));
		sb.append(": ");
		
		// bytes
		for (ByteDumpCharacter character : characters)
		{
			String byteString = character.asByteString(getSettings().getFormat().getMaxLength() + 1);
			sb.append(byteString);
		}
		
		// characters
		sb.append("|");
		for (ByteDumpCharacter character : characters)
		{
			sb.append(character.asReadableCharacter());
		}
		sb.append("|");
		
		return sb.toString();
	}
}
