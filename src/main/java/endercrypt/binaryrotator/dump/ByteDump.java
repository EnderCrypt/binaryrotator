/************************************************************************
 * BinaryRotator by EnderCrypt                                          *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.binaryrotator.dump;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import endercrypt.binaryrotator.dump.builder.ByteDumperSettings;
import endercrypt.binaryrotator.dump.line.ByteDumpLine;
import endercrypt.binaryrotator.dump.utility.CharacterRange;


public class ByteDump
{
	private final ByteDumperSettings settings;
	private List<ByteDumpLine> lines = new ArrayList<>();
	
	public ByteDump(ByteDumperSettings settings)
	{
		this.settings = settings;
		
		CharacterRange range = getSettings().getCharacterRange();
		int radix = getSettings().getFormat().getRadix();
		int rowAmount = getSettings().getRowAmount();
		int rowStart = Math.max(0, (range.getStart() / radix) - rowAmount + 1);
		int maxRow = getSettings().getBytes().getLength() / radix;
		int rowEnd = Math.min(maxRow, (range.getEnd() / radix) + rowAmount - 1);
		for (int row = rowStart; row <= rowEnd; row++)
		{
			lines.add(new ByteDumpLine(getSettings(), row));
		}
	}
	
	public ByteDumperSettings getSettings()
	{
		return settings;
	}
	
	public List<ByteDumpLine> getLines()
	{
		return Collections.unmodifiableList(lines);
	}
	
	public void print()
	{
		System.out.println(this);
	}
	
	@Override
	public String toString()
	{
		return lines.stream()
			.map(ByteDumpLine::toString)
			.collect(Collectors.joining("\n"));
	}
}
